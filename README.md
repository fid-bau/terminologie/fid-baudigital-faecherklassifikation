# FID BAUdigital Fächerklassifikation / FID BAUdigital Subject Classification

In the context of the project FID BAUdigital a new subject classification has been developed for the scientific fields of Civil Engineering, Architecture and Urban Studies with a special section on digitization. The classification is a SKOS vocabulary.

The top-level concepts are:

![images/Individual_Hierarchy.JPG](images/Individual_Hierarchy.JPG)

This subject classification will be used in the future services of FID BAUdigital. It has been mapped to other classification systems like Basisklassifikation of GBV, Regensburger Verbundklassifikation, or FINDEX Raum and FINDEX Bau by Fraunhofer Informationszentrum Raum und Bau IRB.

## Subject Classification

* **Classification with metadata**: [SubjectHeadings_all.owl](SubjectHeadings_all.owl)
* **Ontology PURL**: <https://purl.org/fidbaudigital/subjects>
* **Ontology prefix/id**: `bdsubj`

The file does not contain mappings to external subject classifications or thesauri.

## License

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<br><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""></a></p> 

## Suggestions and contributions

Please see our [Contributors FAQ](/CONTRIBUTING.md) for more information.


## Mappings

The RDF mappings can be reached as seperate files in the directory, but there is also a version of the Subject Classification which contains the mappings.

* **Mapping files (RDF/XML):** [OpenRefine_Output/mappings](OpenRefine_Output/mappings)
* **Classification with metadata and mappings**: [Subject_Headings_all_mappings.owl](Subject_Headings_all_mappings.owl)

These files can be loaded with Protégé by using the following URI: [https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/raw/main/](https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/raw/main/)and adding the directory name (if needed) and the file name to it, e.g. [https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/raw/main/Subject_Headings_all_mappings.owl](https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/raw/main/Subject_Headings_all_mappings.owl).

## Update tabular classification data

The original file of the Subject Classification is a spreadsheet which will not be maintained in the future [tabular/FID BAUdigital Fachgliederung IRB final.xlsx](tabular/FID BAUdigital Fachgliederung IRB final.xlsx).

It has been processed with OpenRefine and exported to a tab-delimited txt file [tabular/SubjectHeadings.txt](tabular/SubjectHeadings.txt).

This file can be imported to OpenRefine to export RDF-based representations of the Subject Classification and its mappings.

Updates and modifications on the Subject Classification and its mappings should be done in this tab-delimited text file. It can be loaded with text editors, OpenRefine, or spreadsheet tools like Excel. When working with Excel please consider the following instructions for textual data import and export [docs/working_with_tabular.md](docs/working_with_tabular.md).

When working on the file with OpenRefine use the defined custom export settings for tabular data: [OpenRefine_GREL/tabular_export.json](OpenRefine_GREL/tabular_export.json).

## Create RDF files from tabular data

Load [tabular/SubjectHeadings.txt](tabular/SubjectHeadings.txt) with OpenRefine and export RDF via its RDF extension.

In order to do so, you need to provide OpenRefine with an RDF skeleton defining which columns will be mapped to ontology constructs (e.g. from OWL or SKOS), what datatypes they have etc.

You can use the predefined settings to create the corresponding files:

|setting|output|
|-|-|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings.json](OpenRefine_GREL/RDF_schema_SubjectHeadings.json)|[OpenRefine_Output/SubjectHeadings.rdf](OpenRefine_Output/SubjectHeadings.rdf)|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings2BK.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2BK.json)|[OpenRefine_Output/mappings/SubjectHeadings2BK.rdf](OpenRefine_Output/mappings/SubjectHeadings2BK.rdf)|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings2RVK.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2RVK.json)|[OpenRefine_Output/mappings/SubjectHeadings2RVK.rdf](OpenRefine_Output/mappings/SubjectHeadings2RVK.rdf)|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings2DDC.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2DDC.json)|[OpenRefine_Output/mappings/SubjectHeadings2DDC.rdf](OpenRefine_Output/mappings/SubjectHeadings2DDC.rdf)|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings2bau.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2bau.json)|[OpenRefine_GREL/RDF_schema_SubjectHeadings2bau.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2bau.json)|
|[OpenRefine_GREL/RDF_schema_SubjectHeadings2raum.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2raum.json)|[OpenRefine_GREL/RDF_schema_SubjectHeadings2raum.json](OpenRefine_GREL/RDF_schema_SubjectHeadings2raum.json)|
|||

## Join RDF files with scripts

The RDF exports from OpenRefine do not yet contain metadata. These are defined in [metadata.owl](metadata.owl). The raw RDF exports from OpenRefine and the metadata file can be joined via scripts.
The script uses rdflib, loads the RDF files as graphs, joins the graphs, serializes them as RDF/XML and saves them to file.

|script|output|
|-|-|
|[scripts/create_Subject_Headings_all.py](scripts/create_Subject_Headings_all.py)|[SubjectHeadings_all.owl](SubjectHeadings_all.owl)|
|[scripts/create_Subject_Headings_all_mappings.py](scripts/create_Subject_Headings_all_mappings.py)|[scripts/create_Subject_Headings_all_mappings.owl](scripts/create_Subject_Headings_all_mappings.owl)|
|||

Run the respective script in a python3 environment.

## Version for terminology service integration

For an integration to Ontology Lookup Service (OLS) instances a version with punning of individuals, i.e. instances of skos:Concept is available (see [Subject_Headings_all_punned.owl](Subject_Headings_all_punned.owl)). This distribution does not contain mappings. It is based on the master distribution which can be retrieved via the ontology PURL <https://purl.org/fidbaudigital/subjects>.

This distribution

* describes the SKOS-hierarchy in terms of rdfs:subClassOf between classes
* states instances of skos:notation relations via a newly defined owl:AnnotationProperty.

All changes and additions are added via SPARQL construct in Protégé:

```SPARQL
PREFIX owl: &lt;http://www.w3.org/2002/07/owl#&gt;
PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;
PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;
prefix skos: &lt;http://www.w3.org/2004/02/skos/core#&gt;


CONSTRUCT {
    ?s a owl:Class . 
    ?s rdfs:subClassOf ?y . 
    ?s <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe> ?notation .
    }

WHERE {
    ?s a skos:Concept .
    ?s skos:broader ?y.
    ?s skos:notation ?notation .
    }

```

## Citation

Fraunhofer-Informationszentrum Raum und Bau IRB, Technische Informationsbibliothek (Hgg.): FID BAUdigital Fächerklassifikation, 2021. URL: <https://purl.org/fidbaudigital/subjects>.

## History

All prior versions are available in this repository. Check out the [releases](https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/releases).

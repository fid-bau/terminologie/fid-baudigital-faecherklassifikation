import csv
from pathlib import Path
from typing import Tuple
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, OWL, RDFS

SH_metadata_fn = Path(__file__).parent.parent / 'metadata.owl'
SH_onto_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'SubjectHeadings.rdf'
SH_all_onto_fn = Path(__file__).parent.parent / 'Subject_Headings_all_mappings.owl'
SubjectHeadings2BK_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'mappings' / 'SubjectHeadings2BK.rdf'
SubjectHeadings2RVK_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'mappings' / 'SubjectHeadings2RVK.rdf'
SubjectHeadings2DDC_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'mappings' / 'SubjectHeadings2DDC.rdf'
SubjectHeadings2bau_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'mappings' / 'SubjectHeadings2bau.rdf'
SubjectHeadings2raum_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'mappings' / 'SubjectHeadings2raum.rdf'
# TODO: define more mapping when required

g_metadata = Graph()
g_metadata.parse(str(SH_metadata_fn.absolute()))

g_SH_onto = Graph()
g_SH_onto.parse(str(SH_onto_fn.absolute()))

g_SubjectHeadings2BK = Graph()
g_SubjectHeadings2BK.parse(str(SubjectHeadings2BK_fn.absolute()))

g_SubjectHeadings2RVK = Graph()
g_SubjectHeadings2RVK.parse(str(SubjectHeadings2RVK_fn.absolute()))

g_SubjectHeadings2DDC = Graph()
g_SubjectHeadings2DDC.parse(str(SubjectHeadings2DDC_fn.absolute()))

g_SubjectHeadings2bau = Graph()
g_SubjectHeadings2bau.parse(str(SubjectHeadings2bau_fn.absolute()))

g_SubjectHeadings2raum = Graph()
g_SubjectHeadings2raum.parse(str(SubjectHeadings2raum_fn.absolute()))

# join g_metadata + g_SH_onto graphs + mapping graphs into g_joint
g_joint = Graph() # after the g_SH_onto
g_joint = g_metadata + g_SH_onto + g_SubjectHeadings2BK + g_SubjectHeadings2RVK + g_SubjectHeadings2DDC + g_SubjectHeadings2bau + g_SubjectHeadings2raum

print('\n\nSERIALIZE\n\n')
print(g_joint.serialize())
with open(SH_all_onto_fn, 'w') as SH_all_onto:
    # SH_all_onto.write(str(g_joint.serialize(format="xml")))
    g_joint.serialize(destination=r"Subject_Headings_all_mappings.owl", format="xml")
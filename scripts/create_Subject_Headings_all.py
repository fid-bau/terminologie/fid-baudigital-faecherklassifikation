import csv
from pathlib import Path
from typing import Tuple
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, OWL, RDFS

SH_metadata_fn = Path(__file__).parent.parent / 'metadata.owl'
SH_onto_fn = Path(__file__).parent.parent / 'OpenRefine_Output' / 'SubjectHeadings.rdf'
SH_all_onto_fn = Path(__file__).parent.parent / 'SubjectHeadings_all.owl'

g_metadata = Graph()
g_metadata.parse(str(SH_metadata_fn.absolute()))

g_SH_onto = Graph()
g_SH_onto.parse(str(SH_onto_fn.absolute()))


# join g_metadata + g_SH_onto graphs into g_joint
g_joint = Graph() # after the g_SH_onto
g_joint = g_metadata + g_SH_onto

print('\n\nSERIALIZE\n\n')
print(g_joint.serialize())
with open(SH_all_onto_fn, 'w') as SH_all_onto:
    # SH_all_onto.write(str(g_joint.serialize(format="xml")))
    g_joint.serialize(destination=r"SubjectHeadings_all.owl", format="xml")
# Contributions FAQ

**What kind of contributions can be made?**

You can report errors or technical issues with FID BAUdigital Subject Headings.

**How can I report errors and technical issues?**

To report errors and technical issues, you should file an [issue](https://gitlab.com/fid-bau/terminologie/fid-baudigital-faecherklassifikation/-/issues).

If you leave an issue, make sure to:

* provide a precise description of the problem
* list all or examplary identifiers, if your report or suggestions concern specific concepts
* if possible describe your suggested solution

**What if I want to suggest a new concept or other content changes to FID BAUdigital Subject Headings?**

At the moment, we do not recommend to suggest conceptual changes to FID BAUdigital Subject Headings. We will still evaluate your suggestions but this may take a little time and the suggestions may be rejected.

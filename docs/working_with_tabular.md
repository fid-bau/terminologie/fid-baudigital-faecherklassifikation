# Working with the tabular source of the classification

The source of the classification is a [spreadsheet](../tabular/FID BAUdigital Fachgliederung IRB final.xlsx) by Fraunhofer Informationszentrum Raum und Bau IRB.

This spreadsheet has been transformed with OpenRefine so that all data can be exported to RDF/XML with an RDF extension for OpenRefine. This [transformed spreadsheet](../tabular/SubjectHeadings.txt) is the master file for all updates on the classification. It is a tab-seperated text file encoded as utf-8.

This text file can be modified in any text editor, but it may be more comfortable to load it into a spreadsheet tool where data can be filtered and irrelevant data can be hidden for a better overview. Modifications should be saved as a text file to track changes transparently in an issue-based git-based workflow.

```markdown
Some general hints before you start:

- Fork the project and work on your local clone from your fork.
- Ideally, use issues to define your tasks and use understandable, transparent commit messages.
- Keep up to date with the source if you want to feed changes back to FID BAUdigital via pull requests.
```

Here is a short guide how to work with the file in Excel.

## Reading the spreadsheet with Excel

1. Copy the file path in your local clone of the project.
2. Open Excel.
3. Use Strg + O to open the file via file path.
4. A Text Import Wizard opens. Choose the following options:
    - data type: delimited
    - start import at row: 1
    - file origin: 65001 Unicoe (UTF-8)

![images/text_import_Excel_1.JPG](../images/text_import_Excel_1.JPG)

5. Click `Next` button in dialog. Choose the following options:
   - delimiter: tab
   - treat consecutive delimiters as one: no
   - text qualifier: " (U+0022 Quotation Mark)

![images/text_import_Excel_2.JPG](../images/text_import_Excel_2.JPG)

6. Click `Next` button in Wizard. Choose the following options:

   - Column data format: Text

![images/text_import_Excel_3.JPG](../images/text_import_Excel_3.JPG)

7. Click `Finish`button.

You should be able to see a proper spreadsheet now.

![images/loaded_spreadsheet_1.JPG](../images/loaded_spreadsheet_1.JPG)

This table can be formatted as a table.

![images/loaded_spreadsheet_2.JPG](../images/loaded_spreadsheet_2.JPG)

Here, you can easily filter the data according to your needs.

![/images/loaded_spreadsheet_3.JPG](../images/loaded_spreadsheet_3.JPG)

## Saving changes

1. Remove all filters.
2. Make sure all lines with data have values in columns `notation` and `IRI`.
3. Sort all lines by notation.
4. Go to menu File and choose `Save as`.
5. Choose file format `Text (tab-delemited(*.txt))`.
6. Before saving click button `tools` and choose `web options`.

![images/save_spreadsheet_1.JPG](../images/save_spreadsheet_1.JPG)

7. Go to tab `encodings` and choose `utf-8`

![images/save_spreadsheet_2.JPG](../images/save_spreadsheet_2.JPG)

8. Save file to path [tabular/SubjectHeadings.txt](../tabular/SubjectHeadings.txt).
